package org.agoncal.quarkus.decoder;

import org.agoncal.quarkus.entities.Game;

import javax.json.bind.JsonbBuilder;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 * this class has not been used
 */
public class GameDecoder implements Decoder.Text<Game> {
    /**
     * will be used to transfer a string (JSON) message to a Game object
     * @param s the received message that we want to convert to a Game object
     * @return a transformed Game object
     * @throws DecodeException occurs when the message can't be decoded
     */
    @Override
    public Game decode(String s) throws DecodeException {
        return JsonbBuilder.create().fromJson(s, Game.class);
    }

    /**
     * the following three methods have to be implemented from the Decoder interface
     */
    @Override
    public boolean willDecode(String s) {
        return true;
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }
}
