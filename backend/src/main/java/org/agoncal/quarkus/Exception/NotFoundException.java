package org.agoncal.quarkus.Exception;

public class NotFoundException extends Exception{
    private final String message;

    /**
     * an Exception occurs if the second player is trying to connect and there is no existing game
     * @param message will appear to the user in case the Exception is thrown
     */
    public NotFoundException(String message) {
        this.message = message;
    }

    /**
     * gets the message that will be thrown
     * @return a String message that will be thrown in case the Exception is thrown
     */
    public String getMessage() {
        return message;
    }
}
