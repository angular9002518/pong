package org.agoncal.quarkus.encoder;

import org.agoncal.quarkus.entities.Game;

import javax.json.bind.JsonbBuilder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class GameEncoder implements Encoder.Text<Game> {

    /**
     * a method that will encode a Game object and convert it to its JSON representation and will be sent to the client
     * @param game the object wanted to encode
     * @return a Game object in the JSON form
     * @throws EncodeException occurs when the passed object can't be encoded
     */
    @Override
    public String encode(Game game) throws EncodeException {
        return JsonbBuilder.create().toJson(game);
    }

    /**
     * called when the webSocket is opened
     * @param config The endpoint configuration contains all the information needed during the handshake process for this end point
     */
    @Override
    public void init(EndpointConfig config) {
        System.out.println("MessageEncoder - init method called");
    }

    /**
     * called when the connection is closed
     */
    @Override
    public void destroy() {
        System.out.println("MessageEncoder - destroy method called");
    }
}
