package org.agoncal.quarkus.entities;


public class Ball extends Coordinates{

    private int ballSize;
    private int ballDx;
    private int ballDy;

    /**
     * an entity that defines the ball
     * @param ballX the X-axe
     * @param ballY the Y-axe
     * @param ballSize the size of the ball
     * @param ballDx in which direction and how fast the ball is moving horizontally
     * @param ballDy in which direction and how fast the ball is moving vertically
     */
    public Ball(int ballX, int ballY, int ballSize, int ballDx, int ballDy){
        super(ballX, ballY);
        this.ballSize = ballSize;
        this.ballDx = ballDx;
        this.ballDy = ballDy;
    }

    /**
     * get the size of the ball
     * @return ball size
     */
    public int getBallSize() {
        return ballSize;
    }

    /**
     * get the ball direction on the X-axe
     * @return Direction of the ball
     */
    public int getBallDx() {return ballDx;}

    /**
     * set the ball direction on the X-axe
     * @param ballDx an integer to set where to move the ball horizontally
     */
    public void setBallDx(int ballDx) {
        this.ballDx = ballDx;
    }

    /**
     * get the ball direction on the Y-axe
     * @return Direction of the ball vertically
     */
    public int getBallDy() {
        return ballDy;
    }

    /**
     * set the ball direction on the Y-axe
     * @param ballDy an integer to set where to move the ball vertically
     */
    public void setBallDy(int ballDy) {
        this.ballDy = ballDy;
    }
}
