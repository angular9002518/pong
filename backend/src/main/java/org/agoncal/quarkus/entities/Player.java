package org.agoncal.quarkus.entities;


public class Player extends Coordinates {

    private int x;
    private int y;
    private int playerWidth;
    private int playerHeight;

    /**
     * an entity that defines the players
     * the player class inherits the Coordinate class to locate the player on the board
     * the canvas takes these four parameters to be able to draw it
     * @param x an integer on the X-axe
     * @param y an integer on the Y-axe
     * @param playerWidth the width of the player
     * @param playerHeight the height of the player
     */
    public Player(int x, int y, int playerWidth, int playerHeight){
        super(x, y);
        this.x = x;
        this.y = y;
        this.playerWidth = playerWidth;
        this.playerHeight = playerHeight;
    }

    /**
     * get the x coordinate of the player
     * @return the x coordinate as an integer
     */
    @Override
    public int getX() {
        return x;
    }

    /**
     * set the x coordinate of a player
     * @param x an integer locates the player on the X-axe
     */
    @Override
    public void setX(int x) {
        this.x = x;
    }

    /**
     * get the y coordinate of the player
     * @return the y coordinate as an integer
     */
    @Override
    public int getY() {
        return y;
    }

    /**
     * set the y coordinate of a player
     * @param y an integer locates the player on the Y-axe
     */
    @Override
    public void setY(int y) {
        this.y = y;
    }

    /**
     * get the width of the player
     * @return width of the player as an integer number
     */
    public int getPlayerWidth() {
        return playerWidth;
    }

    /**
     * get the height of a player
     * @return height of a player as an integer
     */
    public int getPlayerHeight() {
        return playerHeight;
    }
}
