package org.agoncal.quarkus.entities;

public class Coordinates {

    private int x;
    private int y;

    /**
     * a protected class that only will be used in this package and all the other entities will inherit from it
     * it takes two integer parameters that will locate the other entities on the board
     * @param x integer X-axe
     * @param y integer Y-axe
     */
    protected Coordinates(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * set where the entity is located horizontally
     * @param x an integer locate the entity on the X-axe
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * get where the entity is located horizontally
     * @return an integer on where the entity is
     */
    public int getX() {
        return x;
    }

    /**
     * set where the entity is located vertically
     * @param y an integer locate the entity on the Y-axe
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * get where the entity is located vertically
     * @return an integer on where the entity is
     */
    public int getY() {
        return y;
    }
}
