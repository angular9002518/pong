package org.agoncal.quarkus.entities;

public class Board{
    private final int boardWidth;
    private final int boardHeight;

    /**
     * an entity that defines the board game
     * @param boardWidth the width of the board
     * @param boardHeight the height of the board
     */
    public Board(int boardWidth, int boardHeight){
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;
    }

    /**
     * get the width of the board
     * @return the width of the board as an integer
     */
    public int getBoardWidth() {
        return boardWidth;
    }

    /**
     * get the height of the board
     * @return the height of the board as an integer
     */
    public int getBoardHeight() {
        return boardHeight;
    }

}
