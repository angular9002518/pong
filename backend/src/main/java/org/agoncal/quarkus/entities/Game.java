package org.agoncal.quarkus.entities;

import org.agoncal.quarkus.services.GamePlay;

public class Game {

    private String gameID;
    private Board board;
    private Ball ball;
    private Player player1;
    private Player player2;
    private Score scoreLeft;
    private Score scoreRight;
    private GamePlay gamePlay = new GamePlay();

    /**
     * a Game class that is used to send data to the client
     */
    public Game(){}

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Ball getBall() {return ball;}

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Score getScoreLeft() { return scoreLeft;}

    public void setScoreLeft(Score scoreLeft) {this.scoreLeft = scoreLeft;}

    public Score getScoreRight() {
        return scoreRight;
    }

    public void setScoreRight(Score scoreRight) {
        this.scoreRight = scoreRight;
    }

    /**
     * a method used to move the ball and then send the new location and the direction to the client
     */
    public void moveBall(){
        this.gamePlay.moveBall(this.ball, this.board, this.player1, this.player2, this.scoreRight, this.scoreLeft);
    }

    /**
     * a method to update the location of the first player (left player) when he moves up
     */
    public void movePlayer1Up(){
        this.gamePlay.movePlayerUp(this.player1);
    }

    /**
     * a method to update the location of the first player (left player) when he moves down
     */
    public void movePlayer1Down(){
        this.gamePlay.movePlayerDown(this.player1, this.board);
    }

    /**
     * a method to update the location of the second player (right player) when he moves up
     */
    public void movePlayer2Up(){
        this.gamePlay.movePlayerUp(this.player2);
    }

    /**
     * a method to update the location of the second player (right player) when he moves down
     */
    public void movePlayer2Down(){
        this.gamePlay.movePlayerDown(this.player2, this.board);
    }
}
