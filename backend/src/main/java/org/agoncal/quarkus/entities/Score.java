package org.agoncal.quarkus.entities;

public class Score extends Coordinates{

    private int score;

    /**
     * an entity to show the score who is winning and who is losing
     * the x and y coordinates will decide where on the board the score will be shown on the board
     * the canvas that will draw the score takes the tree params
     * @param scoreX where the score is located on the X-axe
     * @param scoreY where the score is located on the Y-axe
     * @param score the score
     */
    public Score(int scoreX, int scoreY, int score) {
        super(scoreX, scoreY);
        this.score = score;
    }

    /**
     * get the score value
     * @return the score value
     */
    public int getScore() {
        return score;
    }

    /**
     * set the score value
     * @param score the score as an integer
     */
    public void setScore(int score) {
        this.score = score;
    }
}
