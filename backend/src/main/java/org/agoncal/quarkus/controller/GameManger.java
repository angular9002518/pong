package org.agoncal.quarkus.controller;

import org.agoncal.quarkus.entities.Game;
import org.agoncal.quarkus.services.ConnectService;

import javax.websocket.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameManger {
    private final ConnectService connectService = new ConnectService();
    private Game game = new Game();
    private final ArrayList<Session> sessions = new ArrayList<>();
    private final ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
    private final Map<String, Session> players = new  HashMap<>();

    /**
     * a connect method will be used when the client opens a new connection to the webSocket
     * it looks at the Array sessions, if there is no session open yet,
     * it will create a new Game and add the new session to the Array sessions
     * @param session provided by the framework and contains the current session
     * @throws InterruptedException occurs if the thread in the starGame is interrupted
     */
    public void connect(Session session) throws InterruptedException{
        if(this.sessions.size() == 0){
            this.game = this.connectService.createGame();
            addSession(session);
            players.put("player1" , session);
            push();
        }else{
            addSession(session);
            if(this.players.containsKey("player2")){
                players.put("player1" , session);
                return;
            }
            players.put("player2" , session);
        }
        if(this.sessions.size() >= 2) {
            this.startGame();
        }
    }

    /**
     * add a new session to the Array sessions
     * @param session provided by the framework and contains the current session
     */
    public void addSession(Session session){
        this.sessions.add(session);
    }

    /**
     * remove the current session from the sessions Array
     * @param session provided by the framework, it contains the current session
     */
    public void removeSession(Session session){
        this.sessions.remove(session);
    }

    /**
     * remove a player from the Map players
     * @param key to access the value in Map players
     */
    public void removePlayer(String key){
        players.remove(key);
    }

    /**
     * a thread that will send data every 17 ms to the client
     */
    private void startGame() {
        timer.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    if(sessions.size() == 2)
                        push();
                    else
                        wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 17, TimeUnit.MILLISECONDS);
    }

    /**
     * method to send the data to the client
     */
    private void push() throws InterruptedException {
        this.game.moveBall();
        broadcast(this.game);
    }

    /**
     * a method that will broadcast the data to all the sessions
     */
    private void broadcast(Game game) {
        this.sessions.forEach(s -> {
            s.getAsyncRemote().sendObject(game, result ->  {
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            });
        });
    }

    /**
     * a Map method that gets players
     * @return a Map players
     */
    public Map<String, Session> getPlayers() {
        return players;
    }

    public void setYPlayer1Up() {
        this.game.movePlayer1Up();
    }

    public void setYPlayer1Down() {
        this.game.movePlayer1Down();
    }

    public void setYPlayer2Up() {
        this.game.movePlayer2Up();
    }

    public void setYPlayer2Down() {
        this.game.movePlayer2Down();
    }

}
