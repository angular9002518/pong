package org.agoncal.quarkus.services;

import org.agoncal.quarkus.Exception.NotFoundException;
import org.agoncal.quarkus.controller.GameManger;
import org.agoncal.quarkus.decoder.GameDecoder;
import org.agoncal.quarkus.encoder.GameEncoder;
import org.agoncal.quarkus.storage.GameStorage;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@ServerEndpoint(value = "/pong",
        encoders = {GameEncoder.class},
        decoders = {GameDecoder.class})
@ApplicationScoped
public class WebSocket {

    private static final Logger LOG = Logger.getLogger(WebSocket.class);

    private final GameManger gameManger = new GameManger();

    /**
     * called when opening a new session in the client endpoint
     * @param session provided by framework containing current session
     * @throws EncodeException occurs if the encoder couldn't encode
     * @throws InterruptedException occurs if the thread in the startGame method in the GameManger class got interrupted
     */
    @OnOpen
    public void onOpen(Session session) throws EncodeException, IOException, InterruptedException, NotFoundException {
        gameManger.connect(session);
    }

    /**
     * called when the client is sending the server some messages
     * expects two different messages when the player wants to move up or down
     * will check which player is sending which message
     * @param msg from string, and it's the message expected from the client
     * @param session the current session
     */
    @OnMessage
    public void Onmessage(String msg, Session session) {
        if (gameManger.getPlayers().get("player1") == session) {
            if (msg.equals("\"up\"")) {
                gameManger.setYPlayer1Up();
            } else if (msg.equals("\"down\"")) {
                gameManger.setYPlayer1Down();
            }
        }
        if (gameManger.getPlayers().get("player2") == session) {
            if (msg.equals("\"up\"")) {
                gameManger.setYPlayer2Up();
            } else if (msg.equals("\"down\"")) {
                gameManger.setYPlayer2Down();
            }
        }
    }

    /**
     * called when the client closes the connection with the server
     * @param session the current session
     */
    @OnClose
    public void onClose(Session session) throws IOException {
        gameManger.removeSession(session);
        if (gameManger.getPlayers().get("player1") == session) {
            gameManger.removePlayer("player1");
        }else{
            gameManger.removePlayer("player2");
        }
        GameStorage.getInstance().removeGame();
        session.close();
    }

    /**
     * called when an error has occurred
     * @param session the current session
     */
    @OnError
    public void onError(Session session, Throwable throwable) throws IOException {
        gameManger.removeSession(session);
        if (gameManger.getPlayers().get("player1") == session) {
            gameManger.removePlayer("player1");
        }else{
            gameManger.removePlayer("player2");
        }
        session.close();
        GameStorage.getInstance().removeGame();
        LOG.error("onError", throwable);
    }
}

