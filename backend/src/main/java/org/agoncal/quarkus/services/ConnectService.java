package org.agoncal.quarkus.services;

import org.agoncal.quarkus.entities.*;
import org.agoncal.quarkus.storage.GameStorage;

import java.util.UUID;

public class ConnectService {

    /**
     * a method from the type Game
     * used to create a new Game with the basic value of the new Game
     * the Game will be stored in the GameStorage
     * @return a new Game with its basic values
     */
    public Game createGame(){
        Game game = new Game();
        game.setBoard(new Board(400, 400));
        game.setGameID(UUID.randomUUID().toString());
        game.setPlayer1(new Player(
                5,
                game.getBoard().getBoardHeight()/2 - 45,
                10,
                90));
        game.setPlayer2(new Player(
                game.getBoard().getBoardWidth() - 15,
                game.getBoard().getBoardHeight()/2 - 45,
                10,
                90));
        game.setBall(new Ball(
                200,
                200,
                8,
                1,
                2));
        game.setScoreRight(new Score(
                game.getBoard().getBoardWidth()/2 + 20,
                20,
                0));
        game.setScoreLeft(new Score(
                game.getBoard().getBoardWidth()/2 - 30,
                20,
                0));
        GameStorage.getInstance().setGame(game);
        return game;
    }
}
