package org.agoncal.quarkus.services;

import org.agoncal.quarkus.entities.Ball;
import org.agoncal.quarkus.entities.Board;
import org.agoncal.quarkus.entities.Player;
import org.agoncal.quarkus.entities.Score;

public class GamePlay {

    /**
     * Used to update the ball position
     * @param ball of type Ball, needed to access the ballX, ballY, ballDx and ballDy
     * @param board of type Board, needed to check where the ball is and let the ball move just inside the board
     * @param player1 of type Player, needed to check if player1 did hit the Ball
     * @param player2 of type Player, needed to check if player2 did hit the Ball
     * @param scoreRight of type Score, needed to show the score to both Players and who is winning or losing
     * @param scoreLeft of type Score
     */
    public void moveBall(Ball ball, Board board, Player player1, Player player2, Score scoreRight, Score scoreLeft){

        ball.setX(ball.getX() + ball.getBallDx());
        ball.setY(ball.getY() + ball.getBallDy());

        if(hitPlayer(ball, player1)){
            ball.setX(ball.getX() + 1);
            ball.setBallDx(ball.getBallDx() * -1);
        }
        if(hitPlayer(ball, player2)){
            ball.setX(ball.getX() - 1);
            ball.setBallDx(ball.getBallDx() * -1);
        }
        if(ball.getY() < 0 || ball.getY() > board.getBoardHeight()){
            ball.setBallDy(ball.getBallDy() * -1);
        }

        if(ball.getX() < 0){
            ball.setX(board.getBoardWidth()/2);
            ball.setY(board.getBoardHeight()/2);
            scoreRight.setScore(scoreRight.getScore() + 1);
        }

        if(ball.getX() > board.getBoardWidth()){
            ball.setX(board.getBoardWidth()/2);
            ball.setY(board.getBoardHeight()/2);
            scoreLeft.setScore(scoreLeft.getScore() + 1);
        }
    }

    /**
     * move the Player up as long as the top of the player is not touching the top board
     * @param player of type Player, needed to access the Y of the Player
     */
    public void movePlayerUp(Player player){
        if(player.getY() > 0)
            player.setY(player.getY() - 5);
        else
            player.setY(0);
    }

    /**
     * move the Player down as long as the bottom of the player is not touching the bottom board
     * @param player of type Player, needed to access the Y of the Player
     * @param board of type Board, needed to access the height of the board to know if the player touched the bottom of the board
     */
    public void movePlayerDown(Player player, Board board) {
        if(player.getY()  + player.getPlayerHeight()  < board.getBoardHeight())
            player.setY(player.getY() + 5);
        else
            player.setY(board.getBoardHeight() - player.getPlayerHeight() );
    }

    /**
     * check if a player hit the ball
     * @param ball of type Ball, needed to know where the ball is at the moment
     * @param player of type Player, needed to know where the Player is now
     * @return true, when the player hits the ball
     */
    public boolean hitPlayer(Ball ball, Player player){
        return  player.getX() < ball.getX() + ball.getBallSize() &&
                player.getY() < ball.getY() + ball.getBallSize() &&
                player.getX() + player.getPlayerWidth() > ball.getX() - ball.getBallSize() &&
                player.getY() + player.getPlayerHeight() > ball.getY() + ball.getBallSize();
    }
}
