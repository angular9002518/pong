package org.agoncal.quarkus.storage;


import org.agoncal.quarkus.entities.Game;

import java.util.HashMap;
import java.util.Map;

public class GameStorage {

    private static Map<String, Game> games;
    private static GameStorage instance;

    /**
     * a map storage that will store all the games but in our case we only have one game and two players
     * the map takes string as a key (gameID) and the value is the game itself
     * the GameStorage class is a singleton class so there will be only one object that's why the constructor is private
     */
    private GameStorage (){games = new HashMap<>();}

    /**
     * a method that returns the instance of the GameStorage class
     * @return the instance of the GameStorage class
     */
    public static synchronized GameStorage getInstance() {
        if (instance == null) {
            instance = new GameStorage();
        }
        return instance;
    }

    /**
     * a map method that returns all the games
     * @return map that includes all the games saved in it
     */
    public Map<String, Game> getGames() {
        return games;
    }

    /**
     * saves a new Game into the storage
     * @param game from Type Game
     */
    public void setGame(Game game) {
        games.put(game.getGameID(), game);
    }

    /**
     * Used to remove the game created in case the connection with client closes
     */
    public void  removeGame(){games.remove(getGames().values().stream().findAny().get().getGameID());}
}
