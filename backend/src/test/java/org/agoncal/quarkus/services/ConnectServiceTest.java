package org.agoncal.quarkus.services;

import org.agoncal.quarkus.storage.GameStorage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ConnectServiceTest {

    @Test
    @DisplayName("Check Game values")
    void valuesOfCreatedGame() {
        if (!GameStorage.getInstance().getGames().isEmpty()) {
            Assertions.assertTrue(GameStorage.getInstance().getGames().values().stream().anyMatch(
                    game -> game.getBoard().getBoardHeight() == 400 &&
                            game.getBoard().getBoardWidth() == 400 &&
                            game.getPlayer1().getX() == 5 &&
                            game.getPlayer1().getY() == game.getBoard().getBoardHeight() / 2 - 45 &&
                            game.getPlayer1().getPlayerHeight() == 90 &&
                            game.getPlayer1().getPlayerWidth() == 10 &&
                            game.getPlayer2().getX() == game.getBoard().getBoardWidth() - 15 &&
                            game.getPlayer2().getY() == game.getBoard().getBoardHeight() / 2 - 45 &&
                            game.getPlayer2().getPlayerHeight() == 90 &&
                            game.getPlayer2().getPlayerWidth() == 10 &&
                            game.getScoreLeft().getX() == game.getBoard().getBoardWidth() / 2 - 30 &&
                            game.getScoreLeft().getY() == 20 &&
                            game.getScoreLeft().getScore() == 0 &&
                            game.getScoreRight().getX() == game.getBoard().getBoardWidth() / 2 + 20 &&
                            game.getScoreRight().getY() == 20 &&
                            game.getScoreRight().getScore() == 0
            ));
        }
    }
}
