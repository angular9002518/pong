package org.agoncal.quarkus.services;

import org.agoncal.quarkus.entities.Ball;
import org.agoncal.quarkus.entities.Board;
import org.agoncal.quarkus.entities.Player;
import org.agoncal.quarkus.entities.Score;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class GamePlayTest {

    GamePlay gamePlay;
    Ball ball;
    Board board;
    Player player1;
    Player player2;
    Score scoreRight;
    Score scoreLeft;

    @BeforeEach
    void setUp(){
        this.gamePlay = new GamePlay();
        this.ball = new Ball(50, 50, 8, 1, 1);
        this.board = new Board(100,100);
        this.player1 = new Player(10,10,10,10);
        this.player2 = new Player(100,100,10,10);
        this.scoreRight = new Score(20,20,0);
        this.scoreLeft = new Score(70,20,0);
    }

    @Test
    @DisplayName("move the ball")
    void moveBallTest(){
        this.gamePlay.moveBall(ball, board, player1, player2, scoreRight, scoreLeft);
        Assertions.assertEquals(51, ball.getX());
        Assertions.assertEquals(51, ball.getY());
    }

    @Test
    @DisplayName("player did hit the ball")
    void hitPlayerTest(){
        Assertions.assertTrue(this.gamePlay.hitPlayer(
                new Ball(390, 81, 8, 1, 2),
                new Player(385, 0, 10, 90)));
    }

    @Test
    @DisplayName("move Player up")
    void movePlayerUpTest(){
        this.gamePlay.movePlayerUp(player1);
        Assertions.assertEquals(5, player1.getY());
    }

    @Test
    @DisplayName("move player down")
    void movePlayerDownTest(){
        this.gamePlay.movePlayerDown(player1, board);
        Assertions.assertEquals(15, player1.getY());
    }
}