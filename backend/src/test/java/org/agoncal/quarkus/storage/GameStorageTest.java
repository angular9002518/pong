package org.agoncal.quarkus.storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class GameStorageTest {

    @Test
    @DisplayName("Check that Game Storage has not more than one Instance of typ Game")
    public void gameStorageSize(){
        Assertions.assertTrue(GameStorage.getInstance().getGames().size() < 2);
    }
}
