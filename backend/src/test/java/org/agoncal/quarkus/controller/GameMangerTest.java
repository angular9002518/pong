package org.agoncal.quarkus.controller;

import org.agoncal.quarkus.entities.Game;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.websocket.Session;

class GameMangerTest {

    private GameManger gameManger;
    private Game game;
    private String key;
    private Session session;

    GameMangerTest(Session session) {
        this.session = session;
    }

    @BeforeEach
    void setUp(){
        this.gameManger = new GameManger();
        this.game = new Game();
        this.key = "";
    }

    @Test
    @DisplayName("connect Method")
    void connectTest() throws InterruptedException {
        this.gameManger.connect(this.session);

    }
}