import {Injectable} from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import {GameStorageService} from "./game-storage.service";

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  private socket: WebSocketSubject<any> = webSocket('ws://localhost:8080/pong');

  /**
   * a service that use to connect the client to the server using webSocket
   * @param gameStorage an GameStorage object that needed to store the incoming data
   */
  constructor(private gameStorage: GameStorageService) {
  }

  /**
   * a methode called when the connection between the client and the server has been closed
   */
  closeConnection() {
    this.connect().complete();
    this.socket.unsubscribe();
  }

  /**
   * methode used to send data to the server end point
   * @param msg of typ any but TODO change the msg typ from any to string and see if it's working
   */
  sendMessage(msg: any) {
    this.socket.next(msg);
  }

  /**
   * called when a handshake is made between the server and the clint
   * it will subscribe to the socket and will observe the incoming data
   */
  connect() {
    console.log("connecting to webSocket");
    this.socket.subscribe({
      next: msg => {
        console.log('message received: ' + JSON.stringify(msg));
        if(msg.ball){
          this.gameStorage.ballX = Number (JSON.stringify(msg.ball.x));
          this.gameStorage.ballY = Number (JSON.stringify(msg.ball.y));
          this.gameStorage.ballSize = Number (JSON.stringify(msg.ball.ballSize));
          this.gameStorage.ballDx = Number (JSON.stringify(msg.ball.ballDx));
          this.gameStorage.ballDy = Number (JSON.stringify(msg.ball.ballDy));
        }
        if(msg.board){
          this.gameStorage.boardWidth = Number (JSON.stringify(msg.board.boardWidth));
          this.gameStorage.boardHeight = Number (JSON.stringify(msg.board.boardHeight));
        }
        if(msg.player1){
          this.gameStorage.player1X = Number (JSON.stringify(msg.player1.x));
          this.gameStorage.player1Y = Number (JSON.stringify(msg.player1.y));
          this.gameStorage.player1Width = Number (JSON.stringify(msg.player1.playerWidth));
          this.gameStorage.player1Height = Number (JSON.stringify(msg.player1.playerHeight));
        }
        if(msg.player2){
          this.gameStorage.player2X = Number (JSON.stringify(msg.player2.x));
          this.gameStorage.player2Y = Number (JSON.stringify(msg.player2.y));
          this.gameStorage.player2Width = Number (JSON.stringify(msg.player2.playerWidth));
          this.gameStorage.player2Height = Number (JSON.stringify(msg.player2.playerHeight));
        }
        if(msg.scoreRight){
          this.gameStorage.scoreRightX = Number (JSON.stringify(msg.scoreRight.x));
          this.gameStorage.scoreRightY = Number (JSON.stringify(msg.scoreRight.y));
          this.gameStorage.scoreRightScore = JSON.stringify(msg.scoreRight.score);
        }
        if(msg.scoreLeft){
          this.gameStorage.scoreLeftX = Number (JSON.stringify(msg.scoreLeft.x));
          this.gameStorage.scoreLeftY = Number (JSON.stringify(msg.scoreLeft.y));
          this.gameStorage.scoreLeftScore = JSON.stringify(msg.scoreLeft.score);
        }
      },
      error: err =>  {
        console.log(err);
      },
      complete: () => {
        console.log("completed");
      }
    });
    return this.socket;
  }


}
