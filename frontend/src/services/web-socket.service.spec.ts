import {TestBed} from "@angular/core/testing";
import {WebSocketService} from "./web-socket.service";
import {GameStorageService} from "./game-storage.service";
import {Observable, Subject} from "rxjs";

describe('Web Socket Service', () => {
  let webSocketService: WebSocketService;
  let gameStorageService: GameStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebSocketService]
    });
    webSocketService = TestBed.inject(WebSocketService);
    gameStorageService = TestBed.inject(GameStorageService);
  });

  it('service created', () => {
    expect(webSocketService).toBeDefined();
  });

  it('connecting', () => {

  })
});
