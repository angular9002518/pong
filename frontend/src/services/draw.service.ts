import { Injectable } from '@angular/core';
import {GameStorageService} from "./game-storage.service";

@Injectable({
  providedIn: 'root'
})
export class DrawService {

  /**
   * service used to draw all the shapes needed to make a pong game
   * @param gameStorage is a param where all the data are stored
   */
  constructor(private gameStorage: GameStorageService) { }

  /**
   * method to draw the ball
   * arc(x, y, size, startAngle, endAngle, direction of the drawing)
   * @param ctx provides the 2D rendering context for the drawing surface of a <canvas> element.
   * It is used for drawing shapes, text, images, and other objects.
   */
  drawBall(ctx: CanvasRenderingContext2D){
    ctx.beginPath();
    ctx.fillStyle = 'white';
    ctx.arc(this.gameStorage.ballX, this.gameStorage.ballY,this.gameStorage.ballSize, 0, Math.PI * 2, false);
    ctx.fill();
  }

  /**
   * used to remove all the drawing from a Rectangle
   */
  clear(ctx: CanvasRenderingContext2D){
    ctx.fillStyle = 'white';
    ctx.clearRect(0,0,this.gameStorage.boardWidth,this.gameStorage.boardHeight);
  }

  /**
   * used to draw a player as a Rectangle
   * rect takes four param the x, y, width and height
   */
  drawPlayer1(ctx: CanvasRenderingContext2D){
    ctx.fillStyle = '';
    ctx.fillRect(this.gameStorage.player1X, this.gameStorage.player1Y, this.gameStorage.player1Width, this.gameStorage.player1Height);
  }

  /**
   * used to draw a player as a Rectangle
   */
  drawPlayer2(ctx: CanvasRenderingContext2D){
    ctx.fillStyle = 'white';
    ctx.fillRect(this.gameStorage.player2X, this.gameStorage.player2Y, this.gameStorage.player2Width, this.gameStorage.player2Height);
  }

  /**
   * used to draw the line in the middle of the board
   */
  drawNets(ctx: CanvasRenderingContext2D){
    ctx.fillStyle = 'white';
    for(let i = 0; i < this.gameStorage.boardHeight; i += 15)
      ctx.fillRect(this.gameStorage.boardWidth / 2, i, 2, 5);
  }

  /**
   * used to draw the score of the right player
   */
  drawScoreRight(ctx: CanvasRenderingContext2D){
    ctx.beginPath();
    ctx.font = "24px Arial";
    ctx.fillStyle = 'white';

    ctx.fillText(this.gameStorage.scoreRightScore, this.gameStorage.scoreRightX, this.gameStorage.scoreRightY);
  }

  /**
   * used to draw the score of the left player
   */
  drawScoreLeft(ctx: CanvasRenderingContext2D){
    ctx.beginPath();
    ctx.font = "24px Arial";
    ctx.fillStyle = 'white';
    ctx.fillText(this.gameStorage.scoreLeftScore, this.gameStorage.scoreLeftX, this.gameStorage.scoreLeftY);
  }

}
