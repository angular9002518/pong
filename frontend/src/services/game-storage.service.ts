import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
/**
 * here were all the incoming data will be saved and then will be called where and when it needed
 * we have the following incoming data
 * 1. the ball (x, y, size, dx, dy)
 * 2. first player(x, y, width, height)
 * 3. second player(x, y, width, height)
 * 4. the score of the first player(x, y, point)
 * 5. the score of the second player(x, y, point)
 * The following methods are just setter and getter for this data
 */
export class GameStorageService {

  private _boardWidth: number;
  private _boardHeight: number;
  private _ballX: number;
  private _ballY: number;
  private _ballSize: number;
  private _ballDx: number;
  private _ballDy: number;
  private _player1X: number;
  private _player1Y: number;
  private _player1Width: number;
  private _player1Height: number;
  private _player2X: number;
  private _player2Y: number;
  private _player2Width: number;
  private _player2Height: number;
  private _scoreRightX: number;
  private _scoreRightY: number;
  private _scoreRightScore: string;
  private _scoreLeftX: number;
  private _scoreLeftY: number;
  private _scoreLeftScore: string;

  constructor() { }

  get boardWidth(): number {
    return this._boardWidth;
  }

  set boardWidth(value: number) {
    this._boardWidth = value;
  }

  get boardHeight(): number {
    return this._boardHeight;
  }

  set boardHeight(value: number) {
    this._boardHeight = value;
  }

  get ballX(): number {
    return this._ballX;
  }

  set ballX(value: number) {
    this._ballX = value;
  }

  get ballY(): number {
    return this._ballY;
  }

  set ballY(value: number) {
    this._ballY = value;
  }

  get ballSize(): number {
    return this._ballSize;
  }

  set ballSize(value: number) {
    this._ballSize = value;
  }

  get ballDx(): number {
    return this._ballDx;
  }

  set ballDx(value: number) {
    this._ballDx = value;
  }

  get ballDy(): number {
    return this._ballDy;
  }

  set ballDy(value: number) {
    this._ballDy = value;
  }

  get player1X(): number {
    return this._player1X;
  }

  set player1X(value: number) {
    this._player1X = value;
  }

  get player1Y(): number {
    return this._player1Y;
  }

  set player1Y(value: number) {
    this._player1Y = value;
  }

  get player1Width(): number {
    return this._player1Width;
  }

  set player1Width(value: number) {
    this._player1Width = value;
  }

  get player1Height(): number {
    return this._player1Height;
  }

  set player1Height(value: number) {
    this._player1Height = value;
  }

  get player2X(): number {
    return this._player2X;
  }

  set player2X(value: number) {
    this._player2X = value;
  }

  get player2Y(): number {
    return this._player2Y;
  }

  set player2Y(value: number) {
    this._player2Y = value;
  }

  get player2Width(): number {
    return this._player2Width;
  }

  set player2Width(value: number) {
    this._player2Width = value;
  }

  get player2Height(): number {
    return this._player2Height;
  }

  set player2Height(value: number) {
    this._player2Height = value;
  }


  get scoreRightX(): number {
    return this._scoreRightX;
  }

  set scoreRightX(value: number) {
    this._scoreRightX = value;
  }

  get scoreRightY(): number {
    return this._scoreRightY;
  }

  set scoreRightY(value: number) {
    this._scoreRightY = value;
  }



  get scoreLeftX(): number {
    return this._scoreLeftX;
  }

  set scoreLeftX(value: number) {
    this._scoreLeftX = value;
  }

  get scoreLeftY(): number {
    return this._scoreLeftY;
  }

  set scoreLeftY(value: number) {
    this._scoreLeftY = value;
  }


  get scoreRightScore(): string {
    return this._scoreRightScore;
  }

  set scoreRightScore(value: string) {
    this._scoreRightScore = value;
  }

  get scoreLeftScore(): string {
    return this._scoreLeftScore;
  }

  set scoreLeftScore(value: string) {
    this._scoreLeftScore = value;
  }
}
