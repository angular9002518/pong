import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {DrawService} from "../../services/draw.service";
import {WebSocketService} from "../../services/web-socket.service";


@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css']
})
export class PlayComponent implements OnInit{

  @ViewChild('canvas', {static: true})
  canvas: ElementRef<HTMLCanvasElement>;
  ctx: CanvasRenderingContext2D;

  boardWidth = 400;
  boardHeight = 400;

  constructor(private webSocketService: WebSocketService, private drawService?: DrawService) {
  }

  ngOnInit(): void {
    this.ctx = <CanvasRenderingContext2D> this.canvas.nativeElement.getContext("2d");
  }

  connectToSocket(){
    this.webSocketService.connect();
    this.start();
  }

  start() {
    this.drawService?.clear(this.ctx);
    this.drawService!.drawBall(this.ctx);
    this.drawService?.drawPlayer1(this.ctx);
    this.drawService?.drawPlayer2(this.ctx);
    this.drawService?.drawNets(this.ctx);
    this.drawService?.drawScoreRight(this.ctx);
    this.drawService?.drawScoreLeft(this.ctx);
    requestAnimationFrame(() => this.start());
  }

  @HostListener('document:keydown', ['$event'])
    onArrowDown(event: KeyboardEvent){
      if(event.key === 'ArrowUp')
        this.webSocketService?.sendMessage('up');
      if(event.key === 'ArrowDown')
        this.webSocketService?.sendMessage('down');
  }


  test(){
    this.ctx.fillStyle = "red";
    this.ctx.fillRect(385,0,10,90);
    this.ctx.arc(390,81,8,Math.PI * 2,0,false);
    this.ctx.fill();
  }
}
