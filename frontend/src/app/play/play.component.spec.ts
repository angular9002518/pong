import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayComponent } from './play.component';
import {WebSocketService} from "../../services/web-socket.service";
import {webSocket, WebSocketSubject} from "rxjs/webSocket";
import {GameStorageService} from "../../services/game-storage.service";
import {By} from "@angular/platform-browser";

/**
 * Testing for the Play Component
 */

class MockWebSocketService {
  socket: WebSocketSubject<any> = webSocket('ws://localhost:8080/pong');
  connect () {
    this.socket.subscribe({
      next: () => {
        JSON = JSON.parse(
          '{"ball":{"x":201,"y":202,"ballDx":1,"ballDy":2,"ballSize":8},' +
          '"board":{"boardHeight":400,"boardWidth":400},' +
          '"gameID":"08ae7f0a-3097-4ead-9c07-01e511b6f5d2",' +
          '"player1":{"playerHeight":90,"playerWidth":10,"x":5,"y":155},' +
          '"player2":{"playerHeight":90,"playerWidth":10,"x":385,"y":155},' +
          '"scoreLeft":{"x":170,"y":20,"score":0},' +
          '"scoreRight":{"x":220,"y":20,"score":0}}')
      }
    })
  }
}
describe('PlayComponent', () => {
    let component: PlayComponent;
    let webSocketService: WebSocketService;
    let fixture: ComponentFixture<PlayComponent>;
    let el: HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PlayComponent],
            providers: [
              {provide: WebSocketService, useClass: MockWebSocketService}
            ]
        })
            .compileComponents();
        webSocketService = TestBed.inject(WebSocketService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a defined canvas', () => {
    expect(component.canvas).toBeDefined();
  });

  it('should the Context of the Canvas Rendering be defined', () => {
    component.ngOnInit();
    expect(component.ctx).toBeInstanceOf(CanvasRenderingContext2D);
  });

});
